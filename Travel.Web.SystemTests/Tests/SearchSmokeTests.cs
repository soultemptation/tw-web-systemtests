﻿namespace Travel.Web.SystemTests.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using NUnit.Framework;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    using Travel.Web.SystemTests.Bootstrap;
    using Travel.Web.SystemTests.Model;
    using Travel.Web.SystemTests.Pages;

    using Assert = NUnit.Framework.Assert;

    [TestFixture]
    public class SearchSmokeTests
    {
        private DriverFactory driverFactory;

        private IWebDriver driver;

        private WebDriverWait wait;

        private string targetUrl;

        [SetUp]
        public void SetUp()
        {
            driverFactory = new DriverFactory();
            targetUrl = driverFactory.TargetUrl;
            driver = driverFactory.GetDriver();
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
        }

        [TearDown]
        public void TearDown()
        {
            driverFactory.FailTestIfNotSucceeded();
            driver.Quit();
        }

        //[Test]
        public void Citytrip()
        {
            // Run
            var searchResultsPage = PerformCitytripSearch("Wien");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformCitytripSearch("Rom");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformCitytripSearch("Lisabon");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformCitytripSearch("Hamburg");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformCitytripSearch("Prag");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformCitytripSearch("Istambul");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");


            driverFactory.TestSucceeded();
        }

        private CityTripHotelSearchResultsPage PerformCitytripSearch(string destination)
        {
            driver.Navigate().GoToUrl(targetUrl);
            var searchForm = new StartPage(driver, wait).GetCityTripSearchForm();
            searchForm.FillForm(destination, new List<Room> { new Room(2, 0) });
            return searchForm.Search();
        }

        //[Test]
        public void Flight()
        {
            // Run
            var searchResultsPage = PerformFlightSearch("Palma de Mallorca");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformFlightSearch("London");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformFlightSearch("Miami");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformFlightSearch("Los Angeles");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformFlightSearch("Wien");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformFlightSearch("San Francisco");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformFlightSearch("Barcelona");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            driverFactory.TestSucceeded();
        }

        private FlightSearchResultsPage PerformFlightSearch(string destination)
        {
            driver.Navigate().GoToUrl(targetUrl);
            var startPage = new StartPage(driver, wait);
            startPage.SelectEngine(Engine.Flight);

            var searchForm = startPage.GetFlightSearchForm();
            searchForm.FillForm(destination, "Zurich", 2, 0);
            return searchForm.Search();
        }

        //[Test]
        public void Hotel()
        {
            // Run
            var searchResultsPage = PerformHotelSearch("London");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformHotelSearch("Barcelona");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformHotelSearch("München");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformHotelSearch("Palme de Mallorca");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformHotelSearch("Wien");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            // Run
            searchResultsPage = PerformHotelSearch("Venedig");
            // Assert
            Assert.IsTrue(searchResultsPage.ResultItems.Any(), "No search results");

            driverFactory.TestSucceeded();
        }

        private HotelSearchResultsPage PerformHotelSearch(string destination)
        {
            driver.Navigate().GoToUrl(targetUrl);
            var startPage = new StartPage(driver, wait);
            startPage.SelectEngine(Engine.Hotel);

            var searchForm = startPage.GetHotelSearchForm();
            searchForm.FillForm(destination, new List<Room> { new Room(2, 0) });
            return searchForm.Search();
        }
    }
}

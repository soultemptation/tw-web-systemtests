﻿namespace Travel.Web.SystemTests.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using NUnit.Framework;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    using Travel.Web.SystemTests.Bootstrap;
    using Travel.Web.SystemTests.Model;
    using Travel.Web.SystemTests.Modules;
    using Travel.Web.SystemTests.Pages;

    [TestFixture]
    public class Citytrip
    {
        private readonly string browser;

        private readonly string version;

        private readonly string platform;

        private DriverFactory driverFactory;

        private IWebDriver driver;

        private WebDriverWait wait;

        private string targetUrl;

        private Chat chat;

        [SetUp]
        public void SetUp()
        {
            driverFactory = new DriverFactory();
            targetUrl = driverFactory.TargetUrl;
            driver = driverFactory.GetDriver();
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            chat = new Chat(driver);
        }

        [TearDown]
        public void TearDown()
        {
            driverFactory.FailTestIfNotSucceeded();
            driver.Quit();
        }

        [Test]
        public void Citytrip_ToAmsterdam_FromZurich_Room1_1Adults1Child_Room2_1Adult()
        {
            // Run
            driver.Navigate().GoToUrl(targetUrl);
            var startPage = new StartPage(driver, wait);
            startPage.GoToEngine(Engine.CityTrip);


            // Search
            var searchForm = startPage.GetCityTripSearchForm();
            searchForm.FillForm("Amsterdam", new List<Room>{new Room(1, 1), new Room(1, 0)});
            var cityTripHotelSearchResultPage = searchForm.Search();

            // Hotel search result page
            cityTripHotelSearchResultPage.WaitForResults();
            var cityTripHotelConfigurationPage = cityTripHotelSearchResultPage.ProceedWithFirstResult();

            // Hotel configuration page 
            cityTripHotelConfigurationPage.WaitForPageToBeReady();
            var flightConfigurationPage = cityTripHotelConfigurationPage.Proceed();

            // Flight configuration page 
            flightConfigurationPage.WaitForPageToBeReady();

            var outboundElementToSelect = 
                flightConfigurationPage.GetFlightsWithMultipleOutboundSegments()
                .First()
                .GetSecondSegment();
            var inboundElementToSelect = 
                flightConfigurationPage.GetFlightsWithMultipleInboundSegments()
                .First()
                .GetSecondSegment();

            Func<IWebElement> outboundAssert = () => driver.FindElements(By.CssSelector(".js-flightinfo .ov-flight")).ElementAt(0);
            Func<IWebElement> inboundAssert = () => driver.FindElements(By.CssSelector(".js-flightinfo .ov-flight")).ElementAt(1);
            AssertThatSelectingASegmentChangesTheOverview(
                outboundElementToSelect,
                outboundAssert);
            AssertThatSelectingASegmentChangesTheOverview(
                inboundElementToSelect,
                inboundAssert);

            driverFactory.TestSucceeded();
            //flightConfigurationPage.ProceedWithClickInOverview(driver);


        }

        private void AssertThatSelectingASegmentChangesTheOverview(
            IWebElement elementToSelect,
            Func<IWebElement> inboundAssert)
        {
            chat.Remove();

            // Store values for later assertion.

            // XPath: Search ancestor from the input field with class "js-option"
            var option = elementToSelect.FindElement(By.XPath("self::*/ancestor::li[contains(@class,'js-option')]"));
            var departureTime = option.FindElement(By.ClassName("label-time-departure")).Text.Trim();
            var arrivalTime = option.FindElement(By.ClassName("label-time-arrival")).Text.Trim();

            var segment = elementToSelect.FindElement(By.XPath("self::*/ancestor::div[contains(@class,'js-flight-segments')]"));
            var airlineExpected = segment.FindElement(By.CssSelector("img")).GetAttribute("alt").Trim();

            var flight = elementToSelect.FindElement(By.XPath("self::*/ancestor::div[contains(@class,'mod-result-flight')]"));
            var priceExpected = flight.FindElement(By.ClassName("pricecurrent")).Text.Trim();

            // Act
            elementToSelect.Click();

            // Assert
            var elementToAssert = inboundAssert();
            var times = elementToAssert.FindElements(By.CssSelector("time"));
            var airline = elementToAssert.FindElements(By.CssSelector("span")).First().Text.Split(',').ElementAt(0).Trim();
            var price = driver.FindElement(By.CssSelector(".mod-productoverview .pricecurrent")).Text.Trim();
            Assert.AreEqual(departureTime, times.ElementAt(0).Text.Trim());
            Assert.AreEqual(arrivalTime, times.ElementAt(1).Text.Trim());
            Assert.AreEqual(priceExpected, price);

            // Inbound carrier image does not have an alt text. So it can not be tested.
            if (airlineExpected != String.Empty)
            {
                Assert.AreEqual(airlineExpected, airline);  
            }
        }
    }
}

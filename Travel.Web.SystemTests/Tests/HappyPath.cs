﻿namespace Travel.Web.SystemTests.Tests
{
    using System;
    using System.Collections.Generic;

    using NUnit.Framework;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    using Travel.Web.SystemTests.Bootstrap;
    using Travel.Web.SystemTests.Model;
    using Travel.Web.SystemTests.Pages;

    [TestFixture]
    public class HappyPath
    {
        private DriverFactory driverFactory;

        private IWebDriver driver;

        private WebDriverWait wait;

        private string targetUrl;

        [SetUp]
        public void SetUp()
        {
            driverFactory = new DriverFactory();
            targetUrl = driverFactory.TargetUrl;
            driver = driverFactory.GetDriver();
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
        }

        [TearDown]
        public void TearDown()
        {
            driverFactory.FailTestIfNotSucceeded();
            driver.Quit();
        }

        [Test]
        public void HappyPath_Citytrip()
        {
            // Run
            driver.Navigate().GoToUrl(targetUrl);

            // Search
            var startPage = new StartPage(driver, wait);

            var cityTripSearchForm = startPage.GetCityTripSearchForm();
            cityTripSearchForm.FillForm("ber", new List<Room>{new Room(1, 1), new Room(1, 0)});
            var cityTripHotelSearchResultsPage = cityTripSearchForm.Search();
            
            // Hotel search result page
            cityTripHotelSearchResultsPage.WaitForResults();
            var cityTripHotelConfigurationPage = cityTripHotelSearchResultsPage.ProceedWithFirstResult();
            
            // Hotel configuration page 
            cityTripHotelConfigurationPage.WaitForPageToBeReady();
            var flightConfigurationPage = cityTripHotelConfigurationPage.Proceed();

            // Flight configuration page 
            flightConfigurationPage.WaitForPageToBeReady();
            var checkoutTravellersPage = flightConfigurationPage.ProceedWithClickInOverview();

            // Checkout: Travellers
            checkoutTravellersPage.PopulatePassengerData();
            checkoutTravellersPage.PopulateCustomerData();
            var checkoutPaymentPage = checkoutTravellersPage.Proceed();

            // Checkout: Payment
            var checkoutOverviewPage = checkoutPaymentPage.Proceed();
            
            // Checkout: Overview
            checkoutOverviewPage.AcceptGtc();

            // Assert
            driverFactory.TestSucceeded();
        }

        [Test]
        public void HappyPath_Flight()
        {
            // Run
            driver.Navigate().GoToUrl(targetUrl);

            // Search
            var startPage = new StartPage(driver, wait);
            startPage.SelectEngine(Engine.Flight);

            var flightSearchForm = startPage.GetFlightSearchForm();
            flightSearchForm.FillForm("ber", "zurich", 2, 1);
            var flightSearchResultsPage = flightSearchForm.Search();

            // Flight search result page
            flightSearchResultsPage.WaitForResults();
            var checkoutTravellersPage = flightSearchResultsPage.ProceedWithFirstResult();

            // Checkout: Travellers
            checkoutTravellersPage.PopulatePassengerData();
            checkoutTravellersPage.PopulateCustomerData();
            var checkoutPaymentPage = checkoutTravellersPage.Proceed();

            // Checkout: Payment
            var checkoutOverviewPage = checkoutPaymentPage.Proceed();
            
            // Checkout: Overview
            checkoutOverviewPage.AcceptGtc();

            // Assert
            driverFactory.TestSucceeded();
        }

        [Test]
        public void HappyPath_Hotel()
        {
            // Run
            driver.Navigate().GoToUrl(targetUrl);

            // Search
            var startPage = new StartPage(driver, wait);
            startPage.SelectEngine(Engine.Hotel);

            var hotelSearchForm = startPage.GetHotelSearchForm();
            hotelSearchForm.FillForm("ber", new List<Room>{new Room(1, 1), new Room(1, 0)});
            var hotelSearchResultsPage = hotelSearchForm.Search();

            // Hotel search result page
            var hotelConfigurationPage = hotelSearchResultsPage.ProceedWithFirstResult();

            // Hotel configuration page 
            hotelConfigurationPage.WaitForPageToBeReady();
            var checkoutTravellersPage = hotelConfigurationPage.Proceed();

            // Checkout: Travellers
            checkoutTravellersPage.PopulatePassengerData();
            checkoutTravellersPage.PopulateCustomerData();
            var checkoutPaymentPage = checkoutTravellersPage.Proceed();

            // Checkout: Payment
            var checkoutOverviewPage = checkoutPaymentPage.Proceed();

            // Checkout: Overview
            checkoutOverviewPage.AcceptGtc();

            // Assert
            driverFactory.TestSucceeded();
        }
    }
}

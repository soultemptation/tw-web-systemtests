﻿namespace Travel.Web.SystemTests.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using NUnit.Framework;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    using Travel.Web.SystemTests.Bootstrap;
    using Travel.Web.SystemTests.Model;
    using Travel.Web.SystemTests.Modules;
    using Travel.Web.SystemTests.Pages;

    using Assert = NUnit.Framework.Assert;

    [TestFixture]
    public class Flight
    {
        private DriverFactory driverFactory;

        private IWebDriver driver;

        private WebDriverWait wait;

        private string targetUrl;

        private Chat chat;

        [SetUp]
        public void SetUp()
        {
            driverFactory = new DriverFactory();
            targetUrl = driverFactory.TargetUrl;
            driver = driverFactory.GetDriver();
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));

            chat = new Chat(driver);
        }

        [TearDown]
        public void TearDown()
        {
            driverFactory.FailTestIfNotSucceeded();
            driver.Quit();
        }

        [Test]
        public void Flight_ToNewYork_FromZurich_2Adults1Child()
        {
            // Run
            driver.Navigate().GoToUrl(targetUrl);

            var startPage = new StartPage(driver, wait);
            startPage.GoToEngine(Engine.Flight);

            // Search
            //wait.Until(
            //    d =>
            //        {
            //            var count = d.FindElements(By.CssSelector(".mod-search")).Count;
            //            return count == 1;
            //        });
            var searchForm = startPage.GetFlightSearchForm();
            searchForm.FillForm("new york", "Zurich", 2, 1);
            var flightSearchResultsPage = searchForm.Search();

            // Flight search result page
            flightSearchResultsPage.WaitForResults();
            chat.Remove();

            AssertThatChangingRadiosDoesNotAffectOtherFlights(
                flightSearchResultsPage.GetFlightsWithMultipleOutboundSegments());
            AssertThatChangingRadiosDoesNotAffectOtherFlights(
                flightSearchResultsPage.GetFlightsWithMultipleInboundSegments());

            AssertThatTotalpriceIsBeeingDisplayed(flightSearchResultsPage);

            AssertThatDatesAreBeeingDisplayed(flightSearchResultsPage);

            AssertionsForEachSearchResult(flightSearchResultsPage);

            // - Select flight and store information for later assertions.
            var searchFlightData = flightSearchResultsPage.GetFlightDataOfFlightWithMultipleOutboundAndInboundSegments();
            var checkoutTravellersPage = flightSearchResultsPage.SelectFlightWithMultipleOutboundAndInboundSegments();

            // Checkout: Travellers
            var checkoutFlightData = checkoutTravellersPage.GetFlightData();

            Assert.AreEqual(searchFlightData.Price, checkoutFlightData.Price);
            Assert.AreEqual(searchFlightData.OutboundDepartureTime, checkoutFlightData.OutboundDepartureTime);
            Assert.AreEqual(searchFlightData.OutboundArrivalTime, checkoutFlightData.OutboundArrivalTime);
            Assert.AreEqual(searchFlightData.InboundDepartureTime, checkoutFlightData.InboundDepartureTime);
            Assert.AreEqual(searchFlightData.InboundArrivalTime, checkoutFlightData.InboundArrivalTime);

            // Fill in data to be able to proceed.
            checkoutTravellersPage.PopulatePassengerData();
            checkoutTravellersPage.PopulateCustomerData();

            var checkoutPaymentPage = checkoutTravellersPage.Proceed();

            // Checkout: Payment

            AssertThatSelectingTheCCRadioElementShowsTheCCForm(checkoutPaymentPage);

            // Run this test first right after the CC payment has been select, so all field are not populated jet.
            AssertThatFormValidationThrowsErrorsWhenFieldsAreNotSetAndSubmitIsPressed(checkoutPaymentPage);

            AssertCCNumberFieldErrorHandling(checkoutPaymentPage);
            AssertCCMonthFieldErrorHandling(checkoutPaymentPage);
            AssertCCYearFieldErrorHandling(checkoutPaymentPage);
            AssertCCCvcFieldErrorHandling(checkoutPaymentPage);

            // Test deactivated because JIRA TWWEB-570
            //AssertThatSyntacticButInvalidDataShowsAnErrorAndShowsTheCCForm(checkoutPage);

            AssertThatFormValidationThrowsNoErrorsWhenFieldsAreSetCorrectlyAndSubmitIsPressed(checkoutPaymentPage);

            driverFactory.TestSucceeded();
        }

        private void AssertThatSyntacticButInvalidDataShowsAnErrorAndShowsTheCCForm(CheckoutPaymentPage checkoutPaymentPage)
        {
            var nameField = checkoutPaymentPage.inptName;
            checkoutPaymentPage.EnterValueAndTriggerValidation(nameField, "Han Solo");

            var newCheckoutPaymentPage = checkoutPaymentPage.ProceedWithErrors();

            Assert.IsTrue(
                newCheckoutPaymentPage.HasNotification(), 
                "Error message expected but not found when syntactic correct but invalid CC data submitted.");
            Assert.IsTrue(
                newCheckoutPaymentPage.inptSaferpayPayment.Selected, 
                "Expected CC payment selected when an error during CC validation occured, but CC payment is not selected.");
        }

        private void AssertThatFormValidationThrowsNoErrorsWhenFieldsAreSetCorrectlyAndSubmitIsPressed(CheckoutPaymentPage checkoutPaymentPage)
        {
            EnterValueAndCheckForNoError(checkoutPaymentPage.inptNumber, "0123456789012345", checkoutPaymentPage);
            EnterValueAndCheckForNoError(checkoutPaymentPage.inptMonth, "02", checkoutPaymentPage);
            EnterValueAndCheckForNoError(checkoutPaymentPage.inptYear, "21", checkoutPaymentPage);
            EnterValueAndCheckForNoError(checkoutPaymentPage.inptCvc, "123", checkoutPaymentPage);
        }

        private void AssertThatFormValidationThrowsErrorsWhenFieldsAreNotSetAndSubmitIsPressed(CheckoutPaymentPage checkoutPaymentPage)
        {
            try
            {
                checkoutPaymentPage.Proceed();
            }
            catch (Exception)
            {
                return;
            }

            Assert.IsTrue(false, "No error if empty CC form is beeing submitted.");
        }

        private void AssertCCMonthFieldErrorHandling(CheckoutPaymentPage checkoutPaymentPage)
        {
            var monthField = checkoutPaymentPage.inptMonth;

            EnterValueAndCheckForError(monthField, "0", checkoutPaymentPage);
            //EnterValueAndCheckForError(monthField, "00", checkoutPaymentPage);
            EnterValueAndCheckForError(monthField, "8", checkoutPaymentPage);
            //EnterValueAndCheckForError(monthField, "13", checkoutPaymentPage);
            EnterValueAndCheckForNoError(monthField, "01", checkoutPaymentPage);
        }

        private void EnterValueAndCheckForError(IWebElement field, string value, CheckoutPaymentPage checkoutPaymentPage)
        {
            checkoutPaymentPage.EnterValueAndTriggerValidation(field, value);
            CheckForError(field, checkoutPaymentPage);
        }

        private void CheckForError(IWebElement field, CheckoutPaymentPage checkoutPaymentPage)
        {
            Assert.IsTrue(
                checkoutPaymentPage.HasError(field),
                string.Format("No error message for {0} found.", field.GetAttribute("id")));
        }

        private void EnterValueAndCheckForNoError(IWebElement field, string value, CheckoutPaymentPage checkoutPaymentPage)
        {
            checkoutPaymentPage.EnterValueAndTriggerValidation(field, value);
            Assert.IsTrue(
                !checkoutPaymentPage.HasError(field),
                string.Format("Error message for {0} found for input {1}. Expected: No error.", field.GetAttribute("id"), value));
        }

        private void AssertCCYearFieldErrorHandling(CheckoutPaymentPage checkoutPaymentPage)
        {
            var yearField = checkoutPaymentPage.inptYear;

            EnterValueAndCheckForError(yearField, "a", checkoutPaymentPage);
            EnterValueAndCheckForError(yearField, "9", checkoutPaymentPage);
            EnterValueAndCheckForError(yearField, "9a", checkoutPaymentPage);
            EnterValueAndCheckForError(yearField, "a9", checkoutPaymentPage);
            EnterValueAndCheckForNoError(yearField, "20", checkoutPaymentPage);
        }

        private void AssertCCCvcFieldErrorHandling(CheckoutPaymentPage checkoutPaymentPage)
        {
            var cvcField = checkoutPaymentPage.inptCvc;

            EnterValueAndCheckForError(cvcField, "a", checkoutPaymentPage);
            EnterValueAndCheckForError(cvcField, "aa", checkoutPaymentPage);
            EnterValueAndCheckForError(cvcField, "aaa", checkoutPaymentPage);
            EnterValueAndCheckForError(cvcField, "9", checkoutPaymentPage);
            EnterValueAndCheckForError(cvcField, "99", checkoutPaymentPage);
            EnterValueAndCheckForNoError(cvcField, "424", checkoutPaymentPage);
        }

        private void AssertCCNumberFieldErrorHandling(CheckoutPaymentPage checkoutPaymentPage)
        {
            var numberField = checkoutPaymentPage.inptNumber;

            EnterValueAndCheckForError(numberField, "012345678901234", checkoutPaymentPage);
            EnterValueAndCheckForNoError(numberField, "0123456789012345", checkoutPaymentPage);
            
        }

        private void AssertThatSelectingTheCCRadioElementShowsTheCCForm(CheckoutPaymentPage checkoutPaymentPage)
        {
            var numberField = checkoutPaymentPage.inptNumber;
            var monthField = checkoutPaymentPage.inptMonth;
            var yearField = checkoutPaymentPage.inptYear;
            var cvcField = checkoutPaymentPage.inptCvc;
            var nameField = checkoutPaymentPage.inptName;

            Assert.IsFalse(numberField.Displayed, "cc number field is displayed by default.");
            Assert.IsFalse(monthField.Displayed, "cc month field is displayed by default.");
            Assert.IsFalse(yearField.Displayed, "cc year field is displayed by default.");
            Assert.IsFalse(cvcField.Displayed, "cc cvc field is displayed by default.");
            Assert.IsFalse(nameField.Displayed, "cc name field is displayed by default.");

            checkoutPaymentPage.SelectSaferpayPayment();

            Assert.IsTrue(numberField.Displayed, "cc number field is not beeing displayed after radio has been selected");
            Assert.IsTrue(monthField.Displayed, "cc month field is not beeing displayed after radio has been selected");
            Assert.IsTrue(yearField.Displayed, "cc year field is not beeing displayed after radio has been selected");
            Assert.IsTrue(cvcField.Displayed, "cc cvc field is not beeing displayed after radio has been selected");
            Assert.IsTrue(nameField.Displayed, "cc name field is not beeing displayed after radio has been selected");
        }

        private void AssertThatDatesAreBeeingDisplayed(FlightSearchResultsPage flightSearchResultsPage)
        {
            var dateLabels = flightSearchResultsPage.txtDates;
            Assert.True(dateLabels.ElementAt(0).Text.Trim() != "");
            Assert.True(dateLabels.ElementAt(1).Text.Trim() != "");
        }

        private void AssertionsForEachSearchResult(FlightSearchResultsPage flightSearchResultsPage)
        {
            var results = flightSearchResultsPage.GetSearchResults();
            foreach (var result in results)
            {
                Assert.True(result.HasOutboundFlight());
                Assert.True(result.HasInboundFlight());

                var arilineImages = result.GetArilineImages();
                Assert.AreEqual(2, arilineImages.Count);
                Assert.IsTrue(arilineImages.ElementAt(0).IsDisplayed());
                Assert.IsTrue(arilineImages.ElementAt(1).IsDisplayed());
            }
        }

        private void AssertThatTotalpriceIsBeeingDisplayed(FlightSearchResultsPage flightSearchResultsPage)
        {
            var results = flightSearchResultsPage.GetSearchResults();
            foreach (var result in results)
            {
                Assert.NotNull(result.Price);
            }
        }

        private void AssertThatChangingRadiosDoesNotAffectOtherFlights(
            IList<Modules.Flight> flightsWithMultipleSegements)
        {
            // Two Flights with multiple segments are required. If not found, skip the test.
            if (flightsWithMultipleSegements.Count < 2)
            {
                return;
            }

            // First flight. Get the selected segments.
            var firstFlight = flightsWithMultipleSegements.ElementAt(0);
            var segmentBefore = firstFlight.GetSelectedSegement();

            // Second flight. Change selection
            var secondFlight = flightsWithMultipleSegements.ElementAt(1);
            secondFlight.SelectSecondSegment();
            

            // First flight. Get the selected segments again and compare.
            var segmentAfter = firstFlight.RefetchSegment(segmentBefore);

            Assert.IsTrue(segmentAfter.Selected, "Changing radio buttons affected other radio button group.");
        }
    }
}

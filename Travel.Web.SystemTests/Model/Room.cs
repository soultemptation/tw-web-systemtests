﻿namespace Travel.Web.SystemTests.Model
{
    public class Room
    {
        public int NumberOfAdults { get; private set; }

        public int NumberOfChildren { get; private set; }

        public Room(int numberOfAdults, int numberOfChildren)
        {
            NumberOfAdults = numberOfAdults;
            NumberOfChildren = numberOfChildren;
        }
    }
}
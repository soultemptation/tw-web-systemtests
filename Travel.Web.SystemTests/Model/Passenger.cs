﻿namespace Travel.Web.SystemTests.Model
{
    using System;

    public class Passenger
    {
        public Salutation Salutation { get; private set; }
        public string Firstname { get; private set; }
        public string Lastname { get; private set; }
        public DateTime Birthdate { get; private set; }

        public Passenger(Salutation salutation, string firstname, string lastname, DateTime birthdate)
        {
            Salutation = salutation;
            Firstname = firstname;
            Lastname = lastname;
            Birthdate = birthdate;
        }
    }
}
namespace Travel.Web.SystemTests.Model
{
    public class Customer
    {
        public string Firstname { get; private set; }

        public string Lastname { get; private set; }

        public string Street { get; private set; }

        public string PostalCode { get; private set; }

        public string City { get; private set; }

        public string Email { get; private set; }

        public string Phone { get; private set; }

        public Salutation Salutation { get; private set; }

        public Customer(Salutation salutation, string firstname, string lastname, string street, string postalCode, string city, string email, string phone)
        {
            Firstname = firstname;
            Lastname = lastname;
            Street = street;
            PostalCode = postalCode;
            City = city;
            Email = email;
            Phone = phone;
            Salutation = salutation;
        }
    }
}
﻿namespace Travel.Web.SystemTests.Model
{
    public enum Engine
    {
        CityTrip,
        Flight,
        Hotel
    }
}
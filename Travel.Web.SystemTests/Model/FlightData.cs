namespace Travel.Web.SystemTests.Model
{
    public class FlightData
    {
        public string OutboundDepartureTime { get; private set; }

        public string OutboundArrivalTime { get; private set; }

        public string InboundDepartureTime { get; private set; }

        public string InboundArrivalTime { get; private set; }

        public string Price { get; private set; }

        public FlightData(string outboundDepartureTime, 
                          string outboundArrivalTime, 
                          string inboundDepartureTime, 
                          string inboundArrivalTime, 
                          string price)
        {
            OutboundDepartureTime = outboundDepartureTime;
            OutboundArrivalTime = outboundArrivalTime;
            InboundDepartureTime = inboundDepartureTime;
            InboundArrivalTime = inboundArrivalTime;
            Price = price;
        }
    }
}
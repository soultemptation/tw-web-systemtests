﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel.Web.SystemTests.Bootstrap
{
    using NUnit.Core;
    using NUnit.Framework;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    public static class WebDriverWaitExtensions
    {
        public static void ForPageReload(this WebDriverWait wait)
        {
            Func<IWebDriver, bool> pageLoad =
                (d) => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete");

            wait.Until(pageLoad);
        }
    }
}

﻿namespace Travel.Web.SystemTests.Bootstrap.Configuration
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Xml;

    public class ConfigurationSectionHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            var targetUrl = section.SelectSingleNode("targetUrl").InnerText;
            var testRunner = section.SelectSingleNode("testRunner").InnerText;

            var authenticationTypeNode = section.SelectSingleNode("authenticationType");
            var authenticationType = authenticationTypeNode != null ? authenticationTypeNode.InnerText : null;

            return new TestConfiguration(targetUrl, authenticationType, testRunner);
        }
    }
}
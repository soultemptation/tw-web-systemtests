﻿namespace Travel.Web.SystemTests.Bootstrap.Configuration
{
    using System.Collections.Generic;

    public class TestConfiguration
    {
        public string TargetUrl { get; private set; }

        public string AuthenticationType { get; private set; }

        public string TestRunner { get; private set; }

        public TestConfiguration(string targetUrl, string authenticationType, string testRunner)
        {
            TargetUrl = targetUrl;
            AuthenticationType = authenticationType;
            TestRunner = testRunner;
        }
    }
}

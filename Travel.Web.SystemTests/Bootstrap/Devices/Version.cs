﻿namespace Travel.Web.SystemTests.Bootstrap.Devices
{
    public enum Version
    {
        _7,
        _11,
        _25,
        _38,
    }
}
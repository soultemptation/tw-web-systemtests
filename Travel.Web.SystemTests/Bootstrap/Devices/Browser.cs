﻿namespace Travel.Web.SystemTests.Bootstrap.Devices
{
    public enum Browser
    {
        Firefox,
        Chrome,
        InternetExplorer,
        Opera,
        Safari
    }
}
﻿namespace Travel.Web.SystemTests.Bootstrap
{
    using System;
    using System.Configuration;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;

    using Travel.Web.SystemTests.Bootstrap.Configuration;
    using Travel.Web.SystemTests.Bootstrap.Devices;
    using Travel.Web.SystemTests.TestRunner;

    using Platform = Travel.Web.SystemTests.Bootstrap.Devices.Platform;
    using Version = Travel.Web.SystemTests.Bootstrap.Devices.Version;

    public class DriverFactory 
    {
        public string TargetUrl { get { return targetUrl; } }

        private readonly string targetUrl;

        private readonly IWebDriver driver;

        private readonly string authenticationType;

        private readonly ITestRunner testRunner;

        public DriverFactory()
        {
            var config = GetConfiguration();
            targetUrl = config.TargetUrl;
            authenticationType = config.AuthenticationType;

            testRunner = new TestRunnerFactory(GetBrowser(), GetVersion(), GetPlatform(), config.TestRunner)
                .GetTestRunner();

            driver = testRunner.GetDriver();

            Console.WriteLine("Setting timeouts to 60 seconds");
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(60));
            driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(60));


            driver.Manage().Window.Maximize();

            new Authenticator(driver, authenticationType, targetUrl).Authenticate();
        }

        public void TestSucceeded()
        {
            testRunner.TestSucceeded();
        }

        public void FailTestIfNotSucceeded()
        {
            testRunner.FailTestIfNotSucceeded();
        }

        private TestConfiguration GetConfiguration()
        {
            return (TestConfiguration)ConfigurationManager.GetSection("testConfigurations/testConfiguration");
        }

        private static Platform? GetPlatform()
        {
            return Read<Platform>();
        }

        private Version? GetVersion()
        {
            return Read<Version>();
        }

        private Browser? GetBrowser()
        {
            return Read<Browser>();
        }

        private static T? Read<T>() where T : struct
        {
            var value = GetEnvironmentVariable(typeof(T).Name.ToLower());
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            return (T)Enum.Parse(typeof(T), value);
        }

        private static string GetEnvironmentVariable(string name)
        {
            return Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Machine) ??
                   Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Process) ??
                   Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.User);
        }

        public IWebDriver GetDriver()
        {
            return driver;
        }
    }
}
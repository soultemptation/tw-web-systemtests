namespace Travel.Web.SystemTests.Bootstrap
{
    using System;

    using OpenQA.Selenium;

    public class Authenticator
    {
        private readonly IWebDriver driver;

        private readonly string authenticationType;

        private readonly string targetUrl;

        public Authenticator(IWebDriver driver, string authenticationType, string targetUrl)
        {
            this.driver = driver;
            this.authenticationType = authenticationType;
            this.targetUrl = targetUrl;
        }

        public void Authenticate()
        {
            Console.WriteLine("Authentication type: [{0}]", authenticationType);
            switch (authenticationType)
            {
                case "basicAuthentication":
                    driver.Navigate().GoToUrl(targetUrl);
                    driver.Navigate().GoToUrl(targetUrl.Replace("http://", "https://"));
                    break;
                case "netScaler":
                    driver.Navigate().GoToUrl(targetUrl);
                    driver.FindElement(By.CssSelector("#PassWD")).SendKeys("spinnaker");
                    driver.FindElement(By.CssSelector("input[alt=Submit]")).Click();
                    break;
            }
            Console.WriteLine("Authentication completed");
        }
    }
}
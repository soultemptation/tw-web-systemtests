namespace Travel.Web.SystemTests.Pages
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    using Travel.Web.SystemTests.Model;
    using Travel.Web.SystemTests.Modules;

    public class StartPage
    {
        private readonly IWebDriver driver;

        private readonly WebDriverWait wait;

        [FindsBy(How = How.CssSelector, Using = ".CityTrip-search .js-searches-toggle-buttons button[value='Hotel']")]
        private IWebElement btnSelectHotelSearch;

        [FindsBy(How = How.CssSelector, Using = ".CityTrip-search .js-searches-toggle-buttons button[value='Flight']")]
        private IWebElement btnSelectFlightSearch;

        [FindsBy(How = How.CssSelector, Using = ".CityTrip-search .js-searches-toggle-buttons button[value='CityTrip']")]
        private IWebElement btnSelectCityTripSearch;

        [FindsBy(How = How.CssSelector, Using = ".js-change-engine[data-engine='Hotel']")]
        private IWebElement lnkHotelEngine;

        [FindsBy(How = How.CssSelector, Using = ".js-change-engine[data-engine='Flight']")]
        private IWebElement lnkFlightEngine;

        [FindsBy(How = How.CssSelector, Using = ".js-change-engine[data-engine='CityTrip']")]
        private IWebElement lnkCityTripEngine;

        public StartPage(IWebDriver driver, WebDriverWait wait)
        {
            this.wait = wait;
            this.driver = driver;
            wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".CityTrip-search .js-searches-toggle-buttons")));
            PageFactory.InitElements(driver, this);
        }

        public void SelectEngine(Engine engine)
        {
            switch (engine)
            {
                    case Engine.CityTrip:
                        btnSelectCityTripSearch.Click();
                        break;
                    case Engine.Flight:
                        btnSelectFlightSearch.Click();
                        break;
                    case Engine.Hotel:
                        btnSelectHotelSearch.Click();
                        break;
            }
        }

        public void GoToEngine(Engine engine)
        {
            switch (engine)
            {
                    case Engine.CityTrip:
                        lnkCityTripEngine.Click();
                        break;
                    case Engine.Flight:
                        lnkFlightEngine.Click();
                        break;
                    case Engine.Hotel:
                        lnkHotelEngine.Click();
                        break;
            }
        }

        public CityTripSearchForm GetCityTripSearchForm()
        {
            return new CityTripSearchForm(driver, wait);
        }

        public HotelSearchForm GetHotelSearchForm()
        {
            return new HotelSearchForm(driver, wait);
        }

        public FlightSearchForm GetFlightSearchForm()
        {
            return new FlightSearchForm(driver, wait);
        }
    }
}
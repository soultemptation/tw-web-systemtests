namespace Travel.Web.SystemTests.Pages
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;

    using Travel.Web.SystemTests.Modules;

    public abstract class FlightPage
    {
        protected readonly IWebDriver Driver;

        [FindsBy(How = How.ClassName, Using = "js-results-item")]
        protected IList<IWebElement> searchResults;

        protected FlightPage(IWebDriver driver)
        {
            Driver = driver;
        }

        public IList<Flight> GetFlightsWithMultipleOutboundSegments()
        {
            return GetFlightWithMultipleSegments(true);
        }

        public IList<Flight> GetFlightsWithMultipleInboundSegments()
        {
            return GetFlightWithMultipleSegments(false);
        }

        private IList<Flight> GetFlightWithMultipleSegments(bool checkOnlyOutbound)
        {
            var segmentType = checkOnlyOutbound ? "outbound" : "inbound";
            var result =
                searchResults.Select(
                    searchResult => searchResult.FindElements(By.CssSelector("." + segmentType + " label input")))
                    .Where(segmentInputFields => segmentInputFields.Count > 1)
                    .Select(flight => new Flight(Driver, flight))
                    .ToList();

            if (result.Any())
            {
                return result;
            }

            throw new Exception("No flight with multiple [" + segmentType + "] segments found.");
        }
    }
}
namespace Travel.Web.SystemTests.Pages
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    public class CityTripHotelConfigurationPage
    {
        private readonly IWebDriver driver;

        private readonly WebDriverWait wait;

        private readonly HotelConfigurationPage hotelConfigurationPage;

        public CityTripHotelConfigurationPage(IWebDriver driver, WebDriverWait wait)
        {
            this.driver = driver;
            this.wait = wait;
            hotelConfigurationPage = new HotelConfigurationPage(driver, wait);
        }

        public void WaitForPageToBeReady()
        {
            hotelConfigurationPage.WaitForPageToBeReady();
        }

        public FlightConfigurationPage Proceed()
        {
            hotelConfigurationPage.ClickBroceedButton();
            return new FlightConfigurationPage(driver, wait);
        }
    }
}
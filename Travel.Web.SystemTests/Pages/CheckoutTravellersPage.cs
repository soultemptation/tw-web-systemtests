namespace Travel.Web.SystemTests.Pages
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    using Travel.Web.SystemTests.Model;

    public class CheckoutTravellersPage : CheckoutPage
    {
        [FindsBy(How = How.ClassName, Using = "js-total-price")]
        private IWebElement txtPrice;

        [FindsBy(How = How.CssSelector, Using = ".mod-cart .info .date")]
        private IList<IWebElement> divDateElements;

        private readonly List<Passenger> passengers =
            new List<Passenger>
            {
                new Passenger(
                    Salutation.Male,
                    "Han",
                    "Solo",
                    new DateTime(1980, 03, 24)),
                new Passenger(
                    Salutation.Female,
                    "Leia Organa",
                    "Solo",
                    new DateTime(1978, 02, 12)),
                new Passenger(
                    Salutation.Female,
                    "Luke",
                    "Skywalker",
                    new DateTime(1985, 08, 07))
            };

        private readonly Customer customer = new Customer(
            Salutation.Male,
            "Han",
            "Solo",
            "Bederstrasse 66",
            "8002",
            "Z�rich",
            "info@travel.ch",
            "+41 44 200 26 26");

        public CheckoutTravellersPage(IWebDriver driver, WebDriverWait wait) : base(driver, wait)
        {
        }

        public void PopulatePassengerData()
        {
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));

            var rows = driver.FindElements(By.CssSelector(".passenger-data .g-row"));
            var i = 0;
            foreach (var row in rows)
            {
                var passenger = passengers.ElementAt(i);

                new SelectElement(row.FindElement(By.CssSelector("[name='Data.Salutation.Id']"))).SelectByIndex(passenger.Salutation == Salutation.Male ? 1 : 2);
                row.FindElement(By.CssSelector("[name='Data.FirstName']")).SendKeys(passenger.Firstname);
                row.FindElement(By.CssSelector("[name='Data.LastName']")).SendKeys(passenger.Lastname);
                if (row.FindElements(By.CssSelector(".day")).Any())
                {
                    new SelectElement(row.FindElement(By.CssSelector(".day"))).SelectByValue(
                        passenger.Birthdate.Day.ToString());
                    new SelectElement(row.FindElement(By.CssSelector(".month"))).SelectByValue(
                        passenger.Birthdate.Month.ToString());
                    new SelectElement(row.FindElement(By.CssSelector(".year"))).SelectByValue(
                        passenger.Birthdate.Year.ToString());
                }

                i++;
            }

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
        }

        public void PopulateCustomerData()
        {
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));

            var form = driver.FindElement(By.CssSelector(".customer-data"));
            new SelectElement(form.FindElement(By.CssSelector("[name='Data.Salutation.Id']"))).SelectByIndex(customer.Salutation == Salutation.Male ? 1 : 2);
            form.FindElement(By.CssSelector("[name='Data.FirstName']")).SendKeys(customer.Firstname);
            form.FindElement(By.CssSelector("[name='Data.LastName']")).SendKeys(customer.Lastname);
            form.FindElement(By.CssSelector("[name='Data.Street']")).SendKeys(customer.Street);
            form.FindElement(By.CssSelector("[name='Data.PostalCode']")).SendKeys(customer.PostalCode);
            form.FindElement(By.CssSelector("[name='Data.City']")).SendKeys(customer.City);
            form.FindElement(By.CssSelector("[name='Data.Address']")).SendKeys(customer.Email);
            form.FindElement(By.CssSelector("[name='Data.Number']")).SendKeys(customer.Phone);

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
        }

        public override void WaitForPageToBeReady()
        {
            wait.Until(d => d.FindElement(By.CssSelector(".js-cart-loaded")));
        }

        public new CheckoutPaymentPage Proceed()
        {
            base.Proceed();
            return new CheckoutPaymentPage(driver, wait);
        }
    }
}
namespace Travel.Web.SystemTests.Pages
{
    using System.Collections.Generic;
    using System.Linq;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    using Travel.Web.SystemTests.Model;
    using Travel.Web.SystemTests.Modules;

    public abstract class CheckoutPage
    {
        protected readonly IWebDriver driver;

        protected readonly WebDriverWait wait;

        protected readonly Scroll scroll;

        [FindsBy(How = How.ClassName, Using = "js-submit")]
        protected IWebElement BtnProceed;

        [FindsBy(How = How.ClassName, Using = "btn-back")]
        private IWebElement btnBack;

        [FindsBy(How = How.CssSelector, Using = "[name='Data.Accepted']")]
        private IWebElement checkboxGtc;

        [FindsBy(How = How.ClassName, Using = "js-total-price")]
        private IWebElement txtPrice;

        [FindsBy(How = How.CssSelector, Using = ".mod-cart .info .date")]
        private IList<IWebElement> divDateElements;

        protected CheckoutPage(IWebDriver driver, WebDriverWait wait)
        {
            this.driver = driver;
            this.wait = wait;
            scroll = new Scroll(driver);
            WaitForPageToBeReady();
            PageFactory.InitElements(driver, this);
        }

        public abstract void WaitForPageToBeReady();

        public void Back()
        {
            scroll.ByPixel(5000);
            btnBack.Click();
        }

        public FlightData GetFlightData()
        {
            var infoElements = divDateElements
                .Select(element => element.Text)
                .ToList();
            return new FlightData(infoElements.ElementAt(0).Trim(), 
                infoElements.ElementAt(1).Trim(), 
                infoElements.ElementAt(2).Trim(), 
                infoElements.ElementAt(3).Trim(), 
                txtPrice.Text.Trim());
        }
        public void Proceed()
        {
            scroll.ByPixel(5000);
            BtnProceed.Click();
        }
    }
}
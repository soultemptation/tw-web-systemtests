namespace Travel.Web.SystemTests.Pages
{
    using System.Collections.Generic;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Interactions;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    public class HotelSearchResultsPage
    {
        private readonly IWebDriver driver;

        private readonly WebDriverWait wait;

        [FindsBy(How = How.CssSelector, Using = ".js-results-item .js-choose")]
        private IWebElement btnProceed;

        [FindsBy(How = How.CssSelector, Using = ".js-search-results .js-results-item")]
        private IList<IWebElement> listResultItems;

        public IList<IWebElement> ResultItems {
            get
            {
                return listResultItems;
            }
        }

        public HotelSearchResultsPage(IWebDriver driver, WebDriverWait wait)
        {
            this.driver = driver;
            this.wait = wait;
            WaitForResults();
            PageFactory.InitElements(driver, this);
        }

        public void WaitForResults()
        {
            wait.Until(d => d.FindElement(By.ClassName("js-results-item")));
        }

        public HotelConfigurationPage ProceedWithFirstResult()
        {
            //new Actions(driver).MoveToElement(driver.FindElement(By.CssSelector(".js-results-item .js-choose"))).Click().Perform();
            new Actions(driver).MoveToElement(btnProceed).Click().Perform();
            return new HotelConfigurationPage(driver, wait);
        }
    }
}
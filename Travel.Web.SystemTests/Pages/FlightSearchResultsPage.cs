namespace Travel.Web.SystemTests.Pages
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Interactions;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    using Travel.Web.SystemTests.Model;
    using Travel.Web.SystemTests.Modules;

    public class FlightSearchResultsPage : FlightPage
    {
        private readonly WebDriverWait wait;

        private IWebElement searchResultWithMultipleOutboundAndInboundSegments;

        [FindsBy(How = How.CssSelector, Using = ".mod-flightoption .js-choose")]
        private IWebElement btnProceedOfFirstResult;

        [FindsBy(How = How.CssSelector, Using = ".js-search-content .js-results-item")]
        private IList<IWebElement> listResultItems;

        public IList<IWebElement> ResultItems
        {
            get
            {
                return listResultItems;
            }
        }

        [FindsBy(How = How.ClassName, Using = "label-date")]
        public IList<IWebElement> txtDates;

        public FlightSearchResultsPage(IWebDriver driver, WebDriverWait wait)
            : base(driver)
        {
            this.wait = wait;
            WaitForResults();
            PageFactory.InitElements(driver, this);
        }

        public void WaitForResults()
        {
            wait.Until(d => d.FindElement(By.CssSelector(".js-search-content.content-loaded")));
        }

        public IList<FlightSearchResult> GetSearchResults()
        {
            return ResultItems.Select(item => new FlightSearchResult(Driver, item)).ToList();
        }

        public CheckoutTravellersPage ProceedWithFirstResult()
        {
            new Actions(Driver).MoveToElement(btnProceedOfFirstResult).Click().Perform();
            return new CheckoutTravellersPage(Driver, wait);
        }

        public CheckoutTravellersPage SelectFlightWithMultipleOutboundAndInboundSegments()
        {
            GetFlightWithMultipleOutboundAndInboundSegments().FindElement(By.ClassName("js-choose")).Click();
            return new CheckoutTravellersPage(Driver, wait);
        }

        public FlightData GetFlightDataOfFlightWithMultipleOutboundAndInboundSegments()
        {
            var flightResult = GetFlightWithMultipleOutboundAndInboundSegments();

            var outboundOptions = flightResult.FindElements(By.CssSelector(".outbound li.option"));
            var secondOutbound = outboundOptions.ElementAt(1);
            secondOutbound.FindElement(By.CssSelector("input")).Click();

            var inboundOptions = flightResult.FindElements(By.CssSelector(".inbound li.option"));
            var secondInbound = inboundOptions.ElementAt(1);
            secondInbound.FindElement(By.CssSelector("input")).Click();

            var outboundDepartureTime = secondOutbound.FindElement(By.ClassName("label-time-departure")).Text.Trim();
            var outboundArrivalTime = secondOutbound.FindElement(By.ClassName("label-time-arrival")).Text.Trim();
            var inboundDepartureTime = secondInbound.FindElement(By.ClassName("label-time-departure")).Text.Trim();
            var inboundArrivalTime = secondInbound.FindElement(By.ClassName("label-time-arrival")).Text.Trim();
            var price = GetPrice(flightResult);

            return new FlightData(outboundDepartureTime, outboundArrivalTime, inboundDepartureTime, inboundArrivalTime, price);
        }

        private static string GetPrice(IWebElement flightResult)
        {
            var price = flightResult
                .FindElements(By.ClassName("selenium-total-price"))
                .First(element => element.Displayed)
                .Text.Trim();

            // There is a problem with getting the price. Quickfix: Try again.
            if (price == string.Empty)
            {
                price = flightResult.FindElement(By.ClassName("selenium-total-price")).Text.Trim();
            }
            return price;
        }

        private IWebElement GetFlightWithMultipleOutboundAndInboundSegments()
        {
            if (searchResultWithMultipleOutboundAndInboundSegments != null && searchResultWithMultipleOutboundAndInboundSegments.Displayed)
            {
                return searchResultWithMultipleOutboundAndInboundSegments;
            }

            foreach (var searchResult in searchResults)
            {
                if (searchResult.FindElements(By.CssSelector(".outbound label input")).Count >= 2
                    && searchResult.FindElements(By.CssSelector(".inbound label input")).Count >= 2)
                {
                    searchResultWithMultipleOutboundAndInboundSegments = searchResult;
                    return searchResult;
                }
            }

            throw new Exception("No result with multiple out- and inbound flights.");
        }
    }
}
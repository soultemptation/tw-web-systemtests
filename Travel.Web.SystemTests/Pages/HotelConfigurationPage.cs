namespace Travel.Web.SystemTests.Pages
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    public class HotelConfigurationPage
    {
        private readonly IWebDriver driver;

        private readonly WebDriverWait wait;

        [FindsBy(How = How.CssSelector, Using = ".js-configuration-submit")]
        private IWebElement btnProceed;

        public HotelConfigurationPage(IWebDriver driver, WebDriverWait wait)
        {
            this.wait = wait;
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void WaitForPageToBeReady()
        {
            wait.Until(
                d => d.FindElement(By.CssSelector(".js-configuration-submit:not(.btn-loading)")));
        }

        public CheckoutTravellersPage Proceed()
        {
            ClickBroceedButton();
            return new CheckoutTravellersPage(driver, wait);
        }

        public void ClickBroceedButton()
        {
            btnProceed.Click();
        }
    }
}
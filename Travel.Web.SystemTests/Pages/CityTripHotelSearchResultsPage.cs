namespace Travel.Web.SystemTests.Pages
{
    using System.Collections.Generic;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    public class CityTripHotelSearchResultsPage
    {
        private readonly IWebDriver driver;

        private readonly WebDriverWait wait;

        private readonly HotelSearchResultsPage hotelSearchResultPage;

        public IList<IWebElement> ResultItems
        {
            get
            {
                return hotelSearchResultPage.ResultItems;
            }
        }

        public CityTripHotelSearchResultsPage(IWebDriver driver, WebDriverWait wait)
        {
            this.driver = driver;
            this.wait = wait;
            hotelSearchResultPage = new HotelSearchResultsPage(driver, wait);
        }

        public void WaitForResults()
        {
            hotelSearchResultPage.WaitForResults();
        }

        public CityTripHotelConfigurationPage ProceedWithFirstResult()
        {
            hotelSearchResultPage.ProceedWithFirstResult();
            return new CityTripHotelConfigurationPage(driver, wait);
        }
    }
}
namespace Travel.Web.SystemTests.Pages
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    public class CheckoutOverviewPage : CheckoutPage
    {

        [FindsBy(How = How.CssSelector, Using = "[name='Data.Accepted']")]
        private IWebElement checkboxGtc;

        public CheckoutOverviewPage(IWebDriver driver, WebDriverWait wait)
            : base(driver, wait)
        {
        }

        public override void WaitForPageToBeReady()
        {
            wait.Until(d => d.FindElement(By.CssSelector(".mod-acceptgtc")));
            wait.Until(d => d.FindElement(By.CssSelector(".js-cart-loaded")));
        }

        public void AcceptGtc()
        {
            checkboxGtc.Click();
        }
    }
}
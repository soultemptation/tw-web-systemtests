namespace Travel.Web.SystemTests.Pages
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    public class CheckoutPaymentPage : CheckoutPage
    {
        [FindsBy(How = How.CssSelector, Using = "input[data-hide=\"js-saferpay\"]")]
        public IWebElement inptSaferpayPayment;

        public IWebElement inptNumber
        {
            get
            {
                return driver.FindElement(By.Id("cc_number"));
            }
        }

        public IWebElement inptMonth
        {
            get
            {
                return driver.FindElement(By.Id("cc_exp_month"));
            }
        }

        public IWebElement inptYear
        {
            get
            {
                return driver.FindElement(By.Id("cc_exp_year"));
            }
        }

        public IWebElement inptCvc
        {
            get
            {
                return driver.FindElement(By.Id("cc_cvc"));
            }
        }

        public IWebElement inptName
        {
            get
            {
                return driver.FindElement(By.Id("cc_name"));
            }
        }

        public CheckoutPaymentPage(IWebDriver driver, WebDriverWait wait) : base(driver, wait)
        {
        }

        public new CheckoutOverviewPage Proceed()
        {
            base.Proceed();
            if (HasErrors())
            {
                throw new Exception("Invalid data entered");
            }
            return new CheckoutOverviewPage(driver, wait);
        }

        public new CheckoutPaymentPage ProceedWithErrors()
        {
            base.Proceed();
            var checkoutPaymentPage = new CheckoutPaymentPage(driver, wait);
            checkoutPaymentPage.WaitForPageWithErrorToBeReady();
            return checkoutPaymentPage;
        }

        private bool HasErrors()
        {
            if (!inptSaferpayPayment.Selected)
            {
                return false;
            }

            return HasError(inptNumber) 
                || HasError(inptMonth)
                || HasError(inptYear)
                || HasError(inptCvc)
                || HasError(inptName);
        }

        public override void WaitForPageToBeReady()
        {
            wait.Until(d => d.FindElement(By.CssSelector(".js-payment-selector")));
            wait.Until(d => d.FindElement(By.CssSelector(".js-cart-loaded")));
        }

        public void WaitForPageWithErrorToBeReady()
        {
            // Error is beeing loaded asynchronously. Wait for them...
            wait.Until(d => d.FindElement(By.CssSelector(".mod-notification")));
        }

        public bool HasNotification()
        {
            return driver.FindElements(By.CssSelector(".mod-notification")).Any();
        }

        public void SelectSaferpayPayment()
        {
            inptSaferpayPayment.Click();
        }

        public bool HasError(IWebElement field)
        {
            return
                GetParentElementsWithClass(field, "form-group")
                    .First()
                    .GetAttribute("class")
                    .Split(' ')
                    .Contains("js-error");
        }

        public void EnterValueAndTriggerValidation(IWebElement field, string value)
        {
            field.Clear();
            field.SendKeys(value);
            field.SendKeys(Keys.Tab);
        }

        private IEnumerable<IWebElement> GetParentElementsWithClass(IWebElement field, string className)
        {
            return
                field.FindElements(By.XPath("./ancestor::div[contains(concat(' ', @class, ' '), ' " + className + " ')][1]"));
        }
    }
}
﻿    namespace Travel.Web.SystemTests.Pages
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    public class FlightConfigurationPage : FlightPage
    {
        private readonly WebDriverWait wait;

        [FindsBy(How = How.CssSelector, Using = ".js-configuration-submit")]
        private IWebElement btnProceed;

        public FlightConfigurationPage(IWebDriver driver, WebDriverWait wait)
            : base(driver)
        {
            this.wait = wait;
            WaitForPageToBeReady();
            PageFactory.InitElements(driver, this);
        }

        public void WaitForPageToBeReady()
        {
            wait.Until(d => d.FindElement(By.CssSelector(".mod-citytripflightresults")));
            wait.Until(d => d.FindElement(By.CssSelector(".js-configuration-submit:not(.btn-loading)")));
        }

        public CheckoutTravellersPage ProceedWithClickInOverview()
        {
            btnProceed.Click();
            return new CheckoutTravellersPage(Driver, wait);
        }
    }
}
﻿namespace Travel.Web.SystemTests.TestRunner
{
    using System;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;

    using NUnit.Framework;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;

    using Travel.Web.SystemTests.Bootstrap;
    using Travel.Web.SystemTests.Bootstrap.Devices;

    using Platform = Travel.Web.SystemTests.Bootstrap.Devices.Platform;
    using Version = Travel.Web.SystemTests.Bootstrap.Devices.Version;

    public class SauceLabsTestRunner : ITestRunner
    {
        private const string Username = "slang";

        private const string Password = "0a485ee5-7170-44b1-beba-cbf8ed22bbc4";

        private readonly Browser browser;

        private readonly Version version;

        private readonly Platform platform;

        private TravelRemoteWebDriver driver;

        private bool testSucceeded;

        public SauceLabsTestRunner(Browser browser, Version version, Platform platform)
        {
            this.browser = browser;
            this.version = version;
            this.platform = platform;
        }

        public IWebDriver GetDriver()
        {
            // construct the url to sauce labs
            var commandExecutorUri = new Uri("http://ondemand.saucelabs.com/wd/hub");

            // set up the desired capabilities
            var desiredCapabilites = GetDesirecCapabilities();

            // set the desired browser
            desiredCapabilites.SetCapability("platform", GetPlatform());
            desiredCapabilites.SetCapability("username", Username); 
            desiredCapabilites.SetCapability("accessKey", Password);
            desiredCapabilites.SetCapability("screenResolution", "1280x1024");
            desiredCapabilites.SetCapability("name", TestName.Prettify(TestContext.CurrentContext.Test.Name, platform, browser, version));

            // start a new remote web driver session on sauce labs
            driver = new TravelRemoteWebDriver(commandExecutorUri, desiredCapabilites);
            return driver;
        }

        public void TestSucceeded()
        {
            testSucceeded = true;
            SendTestStatus(true);
        }

        private async void SendTestStatus(bool succeeded)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Basic",
                Convert.ToBase64String(
                    Encoding.ASCII.GetBytes(string.Format("{0}:{1}", Username, Password))));

            var requestUri = string.Format("https://saucelabs.com/rest/v1/{0}/jobs/{1}", 
                Username, driver.SessionId);
            var content = string.Format("{{\"passed\":{0}}}", succeeded ? "true" : "false");
            var response = await client.PutAsync(
                requestUri,
                new StringContent(content, Encoding.UTF8, "application/json"));

            var responseContent = response.Content;
            var s = new StringBuilder();
            using (var reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
            {
                // Write the output.
                var s2 = await reader.ReadToEndAsync();
                s.Append(s2);
            }
        }

        public void FailTestIfNotSucceeded()
        {
            if (!testSucceeded)
            {
                SendTestStatus(false);
            }
        }

        private string GetPlatform()
        {
            switch (platform)
            {
                case Platform.Windows7:
                    return "Windows 7";
                case Platform.Windows8:
                    return "Windows 8";
                case Platform.MacOSX10_9:
                    return "OS X 10.9";
            }
            throw new Exception("Unknown plattform " + platform);
        }

        private DesiredCapabilities GetDesirecCapabilities()
        {
            DesiredCapabilities desiredCapabilities;
            switch (browser)
            {
                case Browser.Chrome:
                    desiredCapabilities = DesiredCapabilities.Chrome();
                    break;
                case Browser.Firefox:
                    desiredCapabilities = DesiredCapabilities.Firefox();
                    break;
                case Browser.InternetExplorer:
                    desiredCapabilities = DesiredCapabilities.InternetExplorer();
                    break;
                case Browser.Opera:
                    desiredCapabilities = DesiredCapabilities.Opera();
                    break;
                case Browser.Safari:
                    desiredCapabilities = DesiredCapabilities.Safari();
                    break;
                default:
                    throw new Exception("Unknown Browser " + browser);
            }
            return desiredCapabilities;
        }
    }
}
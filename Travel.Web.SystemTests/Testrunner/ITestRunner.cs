namespace Travel.Web.SystemTests.TestRunner
{
    using OpenQA.Selenium;

    public interface ITestRunner
    {
        IWebDriver GetDriver();

        void TestSucceeded();

        void FailTestIfNotSucceeded();

    }
}
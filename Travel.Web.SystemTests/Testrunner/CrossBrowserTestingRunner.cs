﻿namespace Travel.Web.SystemTests.TestRunner
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;

    using NUnit.Framework;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;

    using Travel.Web.SystemTests.Bootstrap;
    using Travel.Web.SystemTests.Bootstrap.Devices;

    using Platform = Travel.Web.SystemTests.Bootstrap.Devices.Platform;
    using Version = Travel.Web.SystemTests.Bootstrap.Devices.Version;

    public class CrossBrowserTestingRunner : ITestRunner
    {
        private const string Username = "travelwindow";

        private const string Password = "ub09e9a4dd29b094";

        private readonly Browser browser;

        private readonly Version version;

        private readonly Platform platform;

        private TravelRemoteWebDriver driver;

        private bool testSucceeded;

        public CrossBrowserTestingRunner(Browser browser, Version version, Platform platform)
        {
            this.browser = browser;
            this.version = version;
            this.platform = platform;
        }

        public IWebDriver GetDriver()
        {
            // construct the url to sauce labs
            var commandExecutorUri = new Uri("http://hub.crossbrowsertesting.com:80/wd/hub");

            // set up the desired capabilities
            var desiredCapabilites = new DesiredCapabilities();

            desiredCapabilites.SetCapability("build", "1.0");

            //desiredCapabilites.SetCapability("browser_api_name", "Opera27");
            //desiredCapabilites.SetCapability("os_api_name", "Win8.1");


            //desiredCapabilites.SetCapability("browser_api_name", "FF35");
            //desiredCapabilites.SetCapability("os_api_name", "Win7x64-C1");


            desiredCapabilites.SetCapability("browser_api_name", GetBrowser());
            desiredCapabilites.SetCapability("os_api_name", GetPlatform());

            desiredCapabilites.SetCapability("screen_resolution", "1280x1024");
            desiredCapabilites.SetCapability("record_video", "true");
            desiredCapabilites.SetCapability("record_network", "true");
            desiredCapabilites.SetCapability("record_snapshot", "true");

            desiredCapabilites.SetCapability("username", Username);
            desiredCapabilites.SetCapability("password", Password);
            desiredCapabilites.SetCapability("name", TestName.Prettify(TestContext.CurrentContext.Test.Name, platform, browser, version));

            // start a new remote web driver session
            driver = new TravelRemoteWebDriver(commandExecutorUri, desiredCapabilites);
            return driver;
        }

        public void TestSucceeded()
        {
            testSucceeded = true;
            SendTestStatus(true);
        }

        public void FailTestIfNotSucceeded()
        {
            if (!testSucceeded)
            {
                SendTestStatus(false);
            }
        }

        private async void SendTestStatus(bool succeeded)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Basic",
                Convert.ToBase64String(
                    Encoding.ASCII.GetBytes(string.Format("{0}:{1}", Username, Password))));

            var requestUri = string.Format("http://app.crossbrowsertesting.com/api/v3/selenium/{0}",
                driver.SessionId);
            var content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("action", "set_score"),
                new KeyValuePair<string, string>("score",  succeeded ? "pass" : "fail"),
            });
            var response = await client.PutAsync(requestUri,content);

            var responseContent = response.Content;
            var s = new StringBuilder();
            using (var reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
            {
                // Write the output.
                var s2 = await reader.ReadToEndAsync();
                s.Append(s2);
            }
        }

        private string GetPlatform()
        {
            switch (platform)
            {
                case Platform.Windows7:
                    if (browser == Browser.InternetExplorer && version == Version._11)
                    {
                        return "Win7x64-Base";
                    }
                    return "Win7-C2";
                case Platform.Windows8:
                    return "Win8";
                case Platform.MacOSX10_9:
                    return "Mac10.9";
            }
            throw new Exception("Unknown plattform " + platform);
        }

        private string GetBrowser()
        {
            string browserString;
            switch (browser)
            {
                case Browser.Chrome:
                    browserString = "Chrome";
                    break;
                case Browser.Firefox:
                    browserString = "FF";
                    break;
                case Browser.InternetExplorer:
                    browserString = "IE";
                    break;
                case Browser.Opera:
                    browserString = "Opera";
                    break;
                case Browser.Safari:
                    browserString = "Safari";
                    break;
                default:
                    throw new Exception("Unknown Browser " + browser);
            }
            return browserString + version.ToString().Substring(1);
        }
    }
}
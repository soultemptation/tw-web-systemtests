﻿namespace Travel.Web.SystemTests.TestRunner
{
    using System;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;

    using NUnit.Framework;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;

    using Travel.Web.SystemTests.Bootstrap;
    using Travel.Web.SystemTests.Bootstrap.Devices;

    using Platform = Travel.Web.SystemTests.Bootstrap.Devices.Platform;
    using Version = Travel.Web.SystemTests.Bootstrap.Devices.Version;

    public class BrowserStackTestRunner : ITestRunner
    {
        private const string User = "simonlang1";

        private const string Key = "X6qzoorQpsQTa6SH6R8K";

        private readonly Browser browser;

        private readonly Version version;

        private readonly Platform platform;

        private TravelRemoteWebDriver driver;

        private bool testSucceeded;

        public BrowserStackTestRunner(Browser browser, Version version, Platform platform)
        {
            this.browser = browser;
            this.version = version;
            this.platform = platform;
        }

        public IWebDriver GetDriver()
        {
            var commandExecutorUri = new Uri("http://hub.browserstack.com/wd/hub/");

            var desiredCapabilites = GetDesiredCabailities();

            desiredCapabilites.SetCapability("os", GetOs());
            desiredCapabilites.SetCapability("os_version", GetOsVersion());
            desiredCapabilites.SetCapability("browser", GetBrowserName());
            desiredCapabilites.SetCapability("browser_version", GetVersion());
            desiredCapabilites.SetCapability("browserstack.user", User);
            desiredCapabilites.SetCapability("browserstack.key", Key);
            desiredCapabilites.SetCapability("browserstack.debug", "true");
            desiredCapabilites.SetCapability("resolution", "1280x1024");
            desiredCapabilites.SetCapability("name", TestName.Prettify(TestContext.CurrentContext.Test.Name, platform, browser, version));

            // start a new remote web driver session on sauce labs
            driver = new TravelRemoteWebDriver(commandExecutorUri, desiredCapabilites);
            return driver;
        }

        private DesiredCapabilities GetDesiredCabailities()
        {
            switch (browser)
            {
                case Browser.InternetExplorer:
                    return DesiredCapabilities.InternetExplorer();
                case Browser.Chrome:
                    return DesiredCapabilities.Chrome();
                case Browser.Safari:
                    return DesiredCapabilities.Safari();
            }
            throw new Exception(string.Format("Unknown plattform {0} for BrowserStack test runner", platform));
        }

        private string GetVersion()
        {
            switch (version)
            {
                case Version._38:
                    return "38.0";
                default:
                    throw new Exception(string.Format("Version {0} is unknown for BrowserStack test runner.", version));
            }
        }

        public void TestSucceeded()
        {
            testSucceeded = true;
            SendTestStatus(true);
        }

        private async void SendTestStatus(bool succeeded)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Basic",
                Convert.ToBase64String(
                    Encoding.ASCII.GetBytes(string.Format("{0}:{1}", User, Key))));

            var requestUri = string.Format("https://saucelabs.com/rest/v1/{0}/jobs/{1}", 
                User, driver.SessionId);
            var content = string.Format("{{\"passed\":{0}}}", succeeded ? "true" : "false");
            var response = await client.PutAsync(
                requestUri,
                new StringContent(content, Encoding.UTF8, "application/json"));

            var responseContent = response.Content;
            var s = new StringBuilder();
            using (var reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
            {
                // Write the output.
                var s2 = await reader.ReadToEndAsync();
                s.Append(s2);
            }
        }

        public void FailTestIfNotSucceeded()
        {
            if (!testSucceeded)
            {
                SendTestStatus(false);
            }
        }

        private string GetOs()
        {
            switch (platform)
            {
                case Platform.Windows7:
                    return "Windows";
                case Platform.Windows8:
                    return "Windows";
                case Platform.MacOSX10_9:
                    return "OS X";
            }
            throw new Exception(string.Format("Unknown plattform {0} for BrowserStack test runner", platform));
        }

        private string GetOsVersion()
        {
            switch (platform)
            {
                case Platform.Windows7:
                    return "7";
                case Platform.Windows8:
                    return "8";
                case Platform.MacOSX10_9:
                    return "Mavericks";
            }
            throw new Exception(string.Format("Unknown plattform {0} for BrowserStack test runner", platform));
        }

        private string GetBrowserName()
        {
            switch (browser)
            {
                case Browser.Chrome:
                    return "Chrome";
                case Browser.Firefox:
                    return "Firefox";
                case Browser.InternetExplorer:
                    return "IE";
                case Browser.Safari:
                    return "Safari";
                default:
                    throw new Exception(string.Format("Unknown Browser {0} for TestingBot test runner", browser));
            }
        }
    }
}
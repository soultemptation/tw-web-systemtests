namespace Travel.Web.SystemTests.TestRunner
{
    using System;

    using Travel.Web.SystemTests.Bootstrap.Devices;

    using Version = Travel.Web.SystemTests.Bootstrap.Devices.Version;

    public class TestRunnerFactory
    {
        private readonly Browser? browser;

        private readonly Version? version;

        private readonly Platform? platform;

        private readonly string testRunnerName;

        public TestRunnerFactory(Browser? browser, Version? version, Platform? platform, string testRunnerName)
        {
            this.browser = browser;
            this.version = version;
            this.platform = platform;
            this.testRunnerName = testRunnerName;

            if (testRunnerName != "saucelabs" && testRunnerName != "crossbrowsertesting"
                && testRunnerName != "testingbot" && testRunnerName != "browserstack" && testRunnerName != "local")
            {
                throw new ArgumentOutOfRangeException("testRunnerName");
            }
        }

        public ITestRunner GetTestRunner()
        {
            Console.WriteLine("Getting test runner. Browser: [{0}] Version: [{1}] Platform: [{2}] TestRunnerConfig: [{3}]", 
                browser, version, platform, testRunnerName);
            switch (testRunnerName.ToLower().Trim())
            {
                case "saucelabs":
                    return new SauceLabsTestRunner(browser.Value, version.Value, platform.Value);
                case "crossbrowsertesting":
                    return new CrossBrowserTestingRunner(browser.Value, version.Value, platform.Value);
                case "testingbot":
                    return new TestingBotTestRunner(browser.Value, version.Value, platform.Value);
                case "browserstack":
                    return new BrowserStackTestRunner(browser.Value, version.Value, platform.Value);
                case "local":
                    return new LocalTestRunner();
                default:
                    return null;
            }
        }
    }
}
﻿namespace Travel.Web.SystemTests.TestRunner
{
    using System;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;

    using NUnit.Framework;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;

    using Travel.Web.SystemTests.Bootstrap;
    using Travel.Web.SystemTests.Bootstrap.Devices;

    using Platform = Travel.Web.SystemTests.Bootstrap.Devices.Platform;
    using Version = Travel.Web.SystemTests.Bootstrap.Devices.Version;

    public class TestingBotTestRunner : ITestRunner
    {
        private const string Key = "5c2b512e337fb4e7597936c978b2cca2";

        private const string Secret = "cea3270430f6c8135238fcc7c5b5e396";

        private readonly Browser browser;

        private readonly Version version;

        private readonly Platform platform;

        private TravelRemoteWebDriver driver;

        private bool testSucceeded;

        public TestingBotTestRunner(Browser browser, Version version, Platform platform)
        {
            this.browser = browser;
            this.version = version;
            this.platform = platform;
        }

        public IWebDriver GetDriver()
        {
            var commandExecutorUri = new Uri("http://hub.testingbot.com/wd/hub/");

            var desiredCapabilites = GetDesiredCabailities();

            desiredCapabilites.SetCapability("platform", GetPlatform());
            desiredCapabilites.SetCapability("browserName", GetBrowserName());
            desiredCapabilites.SetCapability("version", GetVersion());
            desiredCapabilites.SetCapability("client_key", Key); 
            desiredCapabilites.SetCapability("client_secret", Secret);
            desiredCapabilites.SetCapability("screenResolution", "1280x1024");
            desiredCapabilites.SetCapability("name", TestName.Prettify(TestContext.CurrentContext.Test.Name, platform, browser, version));

            // start a new remote web driver session on sauce labs
            driver = new TravelRemoteWebDriver(commandExecutorUri, desiredCapabilites);
            return driver;
        }

        private DesiredCapabilities GetDesiredCabailities()
        {
            switch (browser)
            {
                case Browser.InternetExplorer:
                    return DesiredCapabilities.InternetExplorer();
                case Browser.Chrome:
                    return DesiredCapabilities.Chrome();
                case Browser.Safari:
                    return DesiredCapabilities.Safari();
            }
            throw new Exception(string.Format("Unknown plattform {0} for TestingBot test runner", platform));
        }

        private string GetVersion()
        {
            switch (version)
            {
                case Version._38:
                    return "37";
                default:
                    throw new Exception(string.Format("Version {0} is unknown for TestingBot test runner.", version));
            }
        }

        public void TestSucceeded()
        {
            testSucceeded = true;
            SendTestStatus(true);
        }

        private async void SendTestStatus(bool succeeded)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Basic",
                Convert.ToBase64String(
                    Encoding.ASCII.GetBytes(string.Format("{0}:{1}", Key, Secret))));

            var requestUri = string.Format("https://saucelabs.com/rest/v1/{0}/jobs/{1}", 
                Key, driver.SessionId);
            var content = string.Format("{{\"passed\":{0}}}", succeeded ? "true" : "false");
            var response = await client.PutAsync(
                requestUri,
                new StringContent(content, Encoding.UTF8, "application/json"));

            var responseContent = response.Content;
            var s = new StringBuilder();
            using (var reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
            {
                // Write the output.
                var s2 = await reader.ReadToEndAsync();
                s.Append(s2);
            }
        }

        public void FailTestIfNotSucceeded()
        {
            if (!testSucceeded)
            {
                SendTestStatus(false);
            }
        }

        private string GetPlatform()
        {
            switch (platform)
            {
                case Platform.Windows7:
                    return "WIN7";
                case Platform.Windows8:
                    return "WIN8";
                case Platform.MacOSX10_9:
                    return "MAVERICKS";
            }
            throw new Exception(string.Format("Unknown plattform {0} for TestingBot test runner", platform));
        }

        private string GetBrowserName()
        {
            switch (browser)
            {
                case Browser.Chrome:
                    return "chrome";
                case Browser.Firefox:
                    return "firefox";
                case Browser.InternetExplorer:
                    return "internet explorer";
                case Browser.Safari:
                    return "safari";
                default:
                    throw new Exception(string.Format("Unknown Browser {0} for TestingBot test runner", browser));
            }
        }
    }
}
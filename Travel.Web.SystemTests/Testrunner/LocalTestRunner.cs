﻿namespace Travel.Web.SystemTests.TestRunner
{
    using System;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Firefox;

    public class LocalTestRunner : ITestRunner
    {

        public IWebDriver GetDriver()
        {
            //return new InternetExplorerDriver();
            //Console.WriteLine("Starting firefox driver");
            //return new FirefoxDriver();
            Console.WriteLine("Starting chrome driver");
            return new ChromeDriver("resources");
        }

        public void TestSucceeded()
        {
        }

        public void FailTestIfNotSucceeded()
        {
        }
    }
}
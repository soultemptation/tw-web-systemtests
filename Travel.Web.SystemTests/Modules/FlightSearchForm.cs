namespace Travel.Web.SystemTests.Modules
{
    using System.Collections.Generic;
    using System.Linq;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    using Travel.Web.SystemTests.Model;
    using Travel.Web.SystemTests.Pages;

    public class FlightSearchForm : SearchForm
    {
        [FindsBy(How = How.CssSelector, Using = ".Flight-search .js-filter-field")]
        private IList<IWebElement> inptSearchField;

        [FindsBy(How = How.CssSelector, Using = ".Flight-search .search.js-search")]
        private IWebElement btnSearch;

        [FindsBy(How = How.CssSelector, Using = ".js-searches-toggle-buttons button[value='Flight']")]
        private IWebElement btnEngineSelector;

        [FindsBy(How = How.CssSelector, Using = ".Flight-search .from-date-filter-content .filter-field-text")]
        private IWebElement inptFromDate;

        [FindsBy(How = How.CssSelector, Using = ".Flight-search .to-date-filter-content .filter-field-text")]
        private IWebElement inptToDate;

        [FindsBy(How = How.CssSelector, Using = ".Flight-search .js-filter-room-label")]
        private IWebElement lnkRoom;

        [FindsBy(How = How.CssSelector, Using = ".Flight-search .room-add")]
        private IWebElement lnkRoomAdd;

        public FlightSearchForm(IWebDriver driver, WebDriverWait wait) : base (driver, wait)
        {
        }

        public void FillForm(
            string toDestination,
            string fromDestiation,
            int numberOfAdults,
            int numberOfChildren)
        {
            EnterAndClickOnFirstSuggestion(inptSearchField.ElementAt(0), fromDestiation);
            EnterAndClickOnFirstSuggestion(inptSearchField.ElementAt(1), toDestination);

            EnterPassengers(numberOfAdults, numberOfChildren);

            SelectDates();
        }

        public FlightSearchResultsPage Search()
        {
            btnSearch.Click();
            return new FlightSearchResultsPage(driver, wait);
        }

        private void EnterPassengers(int numberOfAdults, int numberOfChildren)
        {
            EnterRooms(new List<Room> { new Room(numberOfAdults, numberOfChildren) }, Engine.Flight);
        }

        protected override IWebElement GetRoomLink()
        {
            return lnkRoom;
        }

        protected override IWebElement GetRoomAddLink()
        {
            return lnkRoomAdd;
        }

        protected override IWebElement GetFromDate()
        {
            return inptFromDate;
        }

        protected override IWebElement GetToDate()
        {
            return inptToDate;
        }

        protected override IWebElement GetEngineSelector()
        {
            return btnEngineSelector;
        }
    }
}
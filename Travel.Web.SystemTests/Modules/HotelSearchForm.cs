namespace Travel.Web.SystemTests.Modules
{
    using System.Collections.Generic;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    using Travel.Web.SystemTests.Model;
    using Travel.Web.SystemTests.Pages;

    public class HotelSearchForm : SearchForm
    {
        [FindsBy(How = How.CssSelector, Using = ".Hotel-search .js-filter-field")]
        private IWebElement inptSearchField;

        [FindsBy(How = How.CssSelector, Using = ".Hotel-search .search.js-search")]
        private IWebElement btnSearch;

        [FindsBy(How = How.CssSelector, Using = ".js-searches-toggle-buttons button[value='Hotel']")]
        private IWebElement btnEngineSelector;

        [FindsBy(How = How.CssSelector, Using = ".Hotel-search .from-date-filter-content .filter-field-text")]
        private IWebElement inptFromDate;

        [FindsBy(How = How.CssSelector, Using = ".Hotel-search .to-date-filter-content .filter-field-text")]
        private IWebElement inptToDate;

        [FindsBy(How = How.CssSelector, Using = ".Hotel-search .js-filter-room-label")]
        private IWebElement lnkRoom;

        [FindsBy(How = How.CssSelector, Using = ".Hotel-search .room-add")]
        private IWebElement lnkRoomAdd;

        public HotelSearchForm(IWebDriver driver, WebDriverWait wait) : base (driver, wait)
        {
        }

        public void FillForm(
            string toDestination,
            IEnumerable<Room> rooms)
        {
            EnterAndClickOnFirstSuggestion(inptSearchField, toDestination);

            EnterRooms(rooms, Engine.Hotel);

            SelectDates();
        }

        public HotelSearchResultsPage Search()
        {
            btnSearch.Click();
            return new HotelSearchResultsPage(driver, wait);
        }

        protected override IWebElement GetRoomLink()
        {
            return lnkRoom;
        }

        protected override IWebElement GetRoomAddLink()
        {
            return lnkRoomAdd;
        }

        protected override IWebElement GetFromDate()
        {
            return inptFromDate;
        }

        protected override IWebElement GetToDate()
        {
            return inptToDate;
        }

        protected override IWebElement GetEngineSelector()
        {
            return btnEngineSelector;
        }
    }
}
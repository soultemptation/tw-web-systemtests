﻿namespace Travel.Web.SystemTests.Modules
{
    using OpenQA.Selenium;

    public class Scroll
    {
        private readonly IWebDriver driver;

        public Scroll(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void ByPixel(int pixelCount)
        {
            var js = ((IJavaScriptExecutor)driver);
            js.ExecuteScript("window.scrollTo(0, "+pixelCount+")");
        }
    }
}

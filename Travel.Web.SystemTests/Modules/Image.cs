namespace Travel.Web.SystemTests.Modules
{
    using OpenQA.Selenium;

    public class Image
    {
        private readonly IWebDriver driver;

        private readonly IWebElement image;

        public Image(IWebDriver driver, IWebElement image)
        {
            this.driver = driver;
            this.image = image;
        }

        public bool IsDisplayed()
        {
            return (bool)((IJavaScriptExecutor)driver).ExecuteScript(
   "return arguments[0].complete && " +
   "typeof arguments[0].naturalWidth != \"undefined\" && " +
   "arguments[0].naturalWidth > 0", image);
        }
    }
}
namespace Travel.Web.SystemTests.Modules
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using OpenQA.Selenium;

    public class FlightSearchResult
    {
        private readonly IWebDriver driver;

        private readonly IWebElement item;

        public IWebElement Price
        {
            get
            {
                return item.FindElement(By.ClassName("pricecurrent"));
            }
        }

        public FlightSearchResult(IWebDriver driver, IWebElement item)
        {
            this.driver = driver;
            this.item = item;
        }

        public bool HasOutboundFlight()
        {
            return item.FindElements(By.CssSelector(".js-outbound input")).Count > 0;
        }

        public bool HasInboundFlight()
        {
            return item.FindElements(By.CssSelector(".js-inbound input")).Count > 0;
        }

        public IList<Image> GetArilineImages()
        {
            var imageElements = item.FindElements(By.CssSelector(".js-flightinfodetails img"));
            return imageElements.Select(image => new Image(driver, image)).ToList();
        }
    }
}
namespace Travel.Web.SystemTests.Modules
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using OpenQA.Selenium;

    public class Flight
    {
        private readonly IWebDriver driver;

        private readonly ReadOnlyCollection<IWebElement> segments;

        public Flight(IWebDriver driver, ReadOnlyCollection<IWebElement> segments)
        {
            this.driver = driver;
            this.segments = segments;
        }

        public IWebElement GetSelectedSegement()
        {
            return segments.First(segment => segment.Selected);
        }

        public void SelectSecondSegment()
        {
            segments.ElementAt(1).Click();
        }

        public IWebElement RefetchSegment(IWebElement segmentToRefetch)
        {
            return driver.FindElement(By.Id(segmentToRefetch.GetAttribute("id")));
        }

        public IWebElement GetSecondSegment()
        {
            return segments.ElementAt(1);
        }
    }
}
namespace Travel.Web.SystemTests.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Interactions;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    using Travel.Web.SystemTests.Model;

    public abstract class SearchForm
    {
        protected readonly IWebDriver driver;

        protected readonly WebDriverWait wait;

        [FindsBy(How = How.CssSelector, Using = ".datepick .datepick-cmd-next")]
        protected IWebElement lnkCalendarNext;

        protected SearchForm(IWebDriver driver, WebDriverWait wait)
        {
            this.driver = driver;
            this.wait = wait;
            WaitForInitialization();
            PageFactory.InitElements(driver, this);
        }
        protected abstract IWebElement GetRoomLink();
        
        protected abstract IWebElement GetRoomAddLink();

        protected abstract IWebElement GetToDate();

        protected abstract IWebElement GetFromDate();

        protected abstract IWebElement GetEngineSelector();

        public void WaitForInitialization()
        {
            wait.Until(d => d.FindElement(By.ClassName("js-field-initialized")));
        }

        public void SelectEngine()
        {
            GetEngineSelector().Click();
        }

        protected void SelectDate(IWebElement field, int dayOffset, int monthOffset)
        {
            new Actions(driver).MoveToElement(field).Click().Perform();

            for (var i = 0; i < monthOffset; i++)
            {
                lnkCalendarNext.Click();
            }

            // Select date in the second last displayed month.
            driver.FindElements(By.CssSelector(".datepick .datepick-month.first a")).ElementAt(dayOffset).Click();
        }

        protected void EnterRooms(IEnumerable<Room> rooms, Engine engine)
        {
            GetRoomLink().Click();

            var first = true;
            var childrenDateOffset = 1;
            var roomIndex = 0;
            foreach (var room in rooms)
            {
                if (!first)
                {
                    GetRoomAddLink().Click();
                }
                first = false;

                // Is has to be fetched freshly from the DOM because after each click the element is newly created and will therefore be stale.

                var adultsInput = GetLastRoom(engine).FindElement(By.Id("room-adult-number-" + roomIndex));
                new SelectElement(adultsInput).SelectByValue(room.NumberOfAdults.ToString());

                var childrenInput = GetLastRoom(engine).FindElement(By.Id("room-child-number-" + roomIndex));
                new SelectElement(childrenInput).SelectByValue(room.NumberOfChildren.ToString());

                roomIndex++;

                if (room.NumberOfChildren > 0)
                {
                    var dateElements = GetLastRoom(engine).FindElements(By.ClassName("combodate"));

                    for (var i = 0; i < room.NumberOfChildren; i++)
                    {
                        var dateElement = dateElements.ElementAt(i);
                        new SelectElement(dateElement.FindElement(By.CssSelector(".day")))
                            .SelectByIndex(childrenDateOffset);
                        new SelectElement(dateElement.FindElement(By.CssSelector(".month")))
                            .SelectByIndex(childrenDateOffset);
                        new SelectElement(dateElement.FindElement(By.CssSelector(".year")))
                            .SelectByIndex(childrenDateOffset);

                        childrenDateOffset++;
                    }
                }
            }
        }

        private IWebElement GetLastRoom(Engine engine)
        {
            return driver
                .FindElements(By.CssSelector("." + engine + "-search .js-filter-room-content .room"))
                .Last();
        }

        protected void SelectDates()
        {
            SelectDate(GetFromDate(), 0, 3);
            SelectDate(GetToDate(), 3, 0);
        }

        protected void EnterAndClickOnFirstSuggestion(
            IWebElement autosuggestField,
            string searchText)
        {
            autosuggestField.Clear();
            autosuggestField.SendKeys(searchText);

            // Wait for suggestion list to get visible.
            var suggestionList = wait.Until(
                d =>
                    {
                        var lists = d.FindElements(By.CssSelector(".ui-autocomplete"));
                        if (!lists.Any())
                        {
                            return null;
                        }
                        var visibleList = lists.Where(element => element.Displayed).ToList();
                        return !visibleList.Any() ? null : visibleList.First();
                    });

            // Get first suggestion and click on it.
            var suggestionItem = suggestionList.FindElement(By.XPath(".//*"));
            suggestionItem.Click();
        }
    }
}
﻿namespace Travel.Web.SystemTests.Modules
{
    using OpenQA.Selenium;

    class Chat
    {
        private readonly IWebDriver driver;

        public Chat(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void Remove()
        {
            var js = ((IJavaScriptExecutor)driver);
            js.ExecuteScript("var a=document.getElementById('habla_beta_container_do_not_rely_on_div_classes_or_names');if (a!=null)a.parentNode.removeChild(a)");
        }
    }
}
